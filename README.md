# Exercicio4_IncubadoraDBServer

## Revisão e Exercícios 

### Exercícios 

**14) Confira se completaste as guias discutidas até o momento:**

- [x] √ Accessing Data with JPA https://spring.io/guides/gs/accessing-data-jpa/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula9.git

**15) Complete as guias a seguir:**

- [x] Accessing Relational Data using JDBC with Spring https://spring.io/guides/gs/relational-data-access/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula10.git

- [x] Managing Transactions https://spring.io/guides/gs/managing-transactions/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula11.git

- [x] 16) Remova a "gambiarra" do exercício 5). Desenvolva um repositório para PetType. Integre o controlador e o novo repositório. ✔

``` 
@Controller 
public class PetTypeController { 
    @Autowired
    private PetTypeRepository petTypes; 
        // restante do controlador 
} 
```

- [x] 17) Faça o mesmo com o controlador e o repositório para Specialty. ✔
- [ ] 18) [EXTRA] Acrescente testes automatizados sobre as operações oferecidas pelos novos repositórios. (Pendente)
- [ ] 19) [EXTRA] Acrescente testes automatizados sobre as operações oferecidas pelos novos controladores. (Pendente)
- [ ] 20) [EXTRA] Estude as três variantes de implementação dos repositórios da Spring PetClinic REST. (Em andamento)

*https://github.com/spring-petclinic/spring-petclinic-rest* 


